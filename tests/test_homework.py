import random
import string
import pytest


from fixtures.chrome import chrome_browser
from fixtures.testarena.login import browser
from pages.home_page import HomePage
from pages.messages_page import MessagesPage
from pages.project_page import ProjectPage

administrator_email = 'administrator@testarena.pl'


def generate_random_string(length=10):
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string


def test_successful_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_add_message(browser):
    my_text = generate_random_string(10)

    home_page = HomePage(browser)
    home_page.click_mail_icon()

    messages_page = MessagesPage(browser)
    messages_page.add_message(my_text)
    messages_page.verify_message_added(my_text)


def test_create_new_project(browser):
    project_name = "TombiName" + generate_random_string(5)
    project_prefix = "Tom" + generate_random_string(3)
    project_description = "This is my first project"

    home_page = HomePage(browser)
    home_page.click_tools_icon()

    project_page = ProjectPage(browser)
    project_page.create_new_project(project_name, project_prefix, project_description)


def test_search_for_project(browser):
    new_project_name = "TombiName" + generate_random_string(5)
    project_prefix = "Tom" + generate_random_string(3)
    project_description = "This is my first project"

    home_page = HomePage(browser)
    home_page.click_tools_icon()

    project_page = ProjectPage(browser)
    project_page.create_new_project(new_project_name, project_prefix, project_description)

    project_page.search_for_project(new_project_name)

    assert project_page.is_project_in_list(new_project_name), f"Project '{new_project_name}' was not found in the project list"
