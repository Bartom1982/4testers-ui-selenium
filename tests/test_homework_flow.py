import random
import string
import time

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_project_create_and_verify():
    administrator_email = 'administrator@testarena.pl'
    password = 'sumXQQ72$L'
    administration_section = "[title=Administracja]"
    project_name = "TombiName" + generate_random_string(5)
    prefix_name = "Tom" + generate_random_string(3)
    project_description = "This is my first project"

    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.maximize_window()

    wait = WebDriverWait(browser, 10)

    # Step 1: Login
    browser.get('http://demo.testarena.pl/zaloguj')
    browser.find_element(By.CSS_SELECTOR, '#email').send_keys(administrator_email)
    browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
    browser.find_element(By.CSS_SELECTOR, '#login').click()

    # Step 2: Verify successful login
    user_email = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, ".user-info small")))
    assert administrator_email == user_email.text

    # Step 3: Navigate to the administration section
    browser.find_element(By.CSS_SELECTOR, administration_section).click()

    # Step 4: Create a new project
    browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/add_project']").click()
    browser.find_element(By.CSS_SELECTOR, "input#name").send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, "input#prefix").send_keys(prefix_name)
    browser.find_element(By.CSS_SELECTOR, "textarea#description").send_keys(project_description)
    browser.find_element(By.CSS_SELECTOR, "input#save").click()

    # Step 5: Navigate to the projects section
    browser.find_element(By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/projects']").click()

    # Step 6: Search for the newly created project
    search_bar = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input#search')))
    assert search_bar.is_displayed()

    search_bar.send_keys(project_name)
    browser.find_element(By.ID, "j_searchButton").click()

    # Step 7: Verify the project appears in the search results
    time.sleep(5)
    project_locator = (By.XPATH, f"//table//td/a[contains(text(), '{project_name}')]")
    project_in_list = wait.until(EC.presence_of_element_located(project_locator))
    assert project_in_list.is_displayed(), f"Project '{project_name}' was not found in the project list"

    browser.quit()


def generate_random_string(length=5):
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string
