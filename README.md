# Project @4testers-ui-selenium

## Overview

This project is a test suite for a web application using the Selenium framework. The tests are written in Python and utilize the `pytest` framework for organizing and running the tests. The project includes various fixtures and page object models to facilitate browser interactions and test different functionalities of the web application.

## Dependencies

- Python
- pytest
- Selenium WebDriver
- WebPage used : http://demo.testarena.pl

## Installation

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/Bartom1982/4testers-ui-selenium.git
    cd 4testers-ui-selenium
    ```

2. Install the required dependencies:
    ```sh
    pip install -r requirements.txt
    ```

## Test Structure

The tests are organized into different files and use the Page Object Model (POM) to interact with the web application. Below is a brief overview of the test file and the functionalities it covers.

### `tests/test_homework.py`

This file contains the following tests:

1. **Test Successful Login**
    - Verifies that the user can log in successfully and checks if the logged-in user's email matches the expected administrator email.
    - References:
        ```python:tests/test_homework.py
        startLine: 20
        endLine: 23
        ```

2. **Test Add Message**
    - Generates a random string and adds it as a message.
    - Verifies that the message was added successfully.
    - References:
        ```python:tests/test_homework.py
        startLine: 26
        endLine: 34
        ```

3. **Test Create New Project**
    - Generates a random project name and prefix.
    - Creates a new project with the generated details.
    - References:
        ```python:tests/test_homework.py
        startLine: 37
        endLine: 46
        ```

4. **Test Search for Project**
    - Generates a random project name and prefix.
    - Creates a new project with the generated details.
    - Searches for the newly created project and verifies its presence in the project list.
    - References:
        ```python:tests/test_homework.py
        startLine: 49
        endLine: 62
        ```

## Fixtures

The project uses fixtures to manage browser instances and other setup/teardown tasks. The following fixtures are imported in the test file:

- `chrome_browser` from `fixtures.chrome`
- `browser` from `fixtures.testarena.login`

## Page Objects

The project uses the Page Object Model to represent different pages of the web application. The following page objects are used in the test file:

- `HomePage` from `pages.home_page`
- `MessagesPage` from `pages.messages_page`
- `ProjectPage` from `pages.project_page`

## Running the Tests

To run the tests, use the following command:
pytest tests/test_homework.py

## Conclusion

This project provides a comprehensive test suite for a web application using Selenium and pytest. It includes tests for login functionality, message addition, project creation, and project search. The use of fixtures and page objects makes the tests modular and easy to maintain.

For any questions or contributions, please refer to the repository or contact the project maintainers.