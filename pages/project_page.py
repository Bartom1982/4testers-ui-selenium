import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


class ProjectPage:
    administration_section = (By.CSS_SELECTOR, '[title=Administracja]')
    add_project_button = (By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/add_project']")
    input_project_name = (By.CSS_SELECTOR, "input#name")
    input_project_prefix = (By.CSS_SELECTOR, "input#prefix")
    input_project_description = (By.CSS_SELECTOR, "textarea#description")
    save_project_button = (By.CSS_SELECTOR, "input#save")
    go_to_projects = (By.CSS_SELECTOR, "a[href='http://demo.testarena.pl/administration/projects']")
    project_search_bar = (By.CSS_SELECTOR, "input#search")
    project_search_icon = (By.ID, "j_searchButton")

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def create_new_project(self, project_name, project_prefix, project_description):
        self.wait.until(EC.element_to_be_clickable(self.add_project_button))
        self.browser.find_element(*self.add_project_button).click()
        self.browser.find_element(*self.input_project_name).send_keys(project_name)
        self.browser.find_element(*self.input_project_prefix).send_keys(project_prefix)
        self.browser.find_element(*self.input_project_description).send_keys(project_description)
        self.browser.find_element(*self.save_project_button).click()
        return self
        # test
        # self.wait.until(EC.element_to_be_clickable(self.left_panel_projects))
        # self.browser.find_element(*self.left_panel_projects).click()
        # self.browser.find_element(*self.project_search_bar).send_keys(project_name)
        # self.browser.find_element(*self.project_search_icon).click()

    def go_to_projects_section(self):
        self.browser.find_element(*self.go_to_projects).click()

    def search_for_project(self, project_name):
        self.go_to_projects_section()
        search_bar = self.wait.until(EC.presence_of_element_located(self.project_search_bar))
        search_bar.send_keys(project_name)
        self.browser.find_element(*self.project_search_icon).click()
        return self

    def is_project_in_list(self, project_name):
        project_locator = (By.XPATH, f"//table//td/a[contains(text(), '{project_name}')]")
        try:
            project_in_list = self.wait.until(EC.presence_of_element_located(project_locator))
            return project_in_list.is_displayed()
        except:
            return False
